#include <ncurses.h>
#include <pthread.h>
#include <unistd.h>
#include <cstdlib>

int X, Y;

struct element{
	int x = -1;
	int y = -1;
	element* next = NULL;
	pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	bool collided = false;
	bool visible = true;
	float v_x;
	float v_y;
};

element* circles = NULL;
element** last = &circles;

bool end = false;

float v_factors[7][2] = {{-0.92, 0.38}, {-0.71, 0.71}, {-0.38, 0.92}, {1, 1}, {0.38, 0.92}, {0.71, 0.71}, {0.92, 0.38}}; //v[x][y]

int abs(int val){
	if(val>=0) return val;
	else return -val;
}

void* circle(void* tab){
	element* ptr = new element;
	float x = X/2;
	float y = 0;
	ptr->x = x;
	ptr->y = y;
	*last = ptr;
	last = &(ptr->next);
	ptr->v_x = *((float*) tab);
	ptr->v_y = *((float*) tab + 1);
	pthread_mutex_t* mutex_ptr = &(ptr->mutex);
	//until v^2>v_min^2
	while(((ptr->v_x*ptr->v_x + ptr->v_y*ptr->v_y) > 0.01) && (!end)){
		//print circle
			ptr->x = x;
			ptr->y = y;
		//move
		x += ptr->v_x;
		y += ptr->v_y;
		//bounce
		if((x < 0) || (x > X)){
			ptr->v_x = -(ptr->v_x);
			x += ptr->v_x;
			//change velocity
			ptr->v_x*=0.5;
			ptr->v_y*=0.5;
		}
		if((y < 0) || (y > Y)){
			ptr->v_y = -(ptr->v_y);
			y += ptr->v_y;
			//change velocity
			ptr->v_x*=0.5;
			ptr->v_y*=0.5;
		}
		//sleep
		usleep(40000);

		//check collision
		element* ptr2 = circles;
		while(ptr2!=NULL){
			if((ptr != ptr2) && (ptr->visible) && (ptr2->visible) && (ptr->x == ptr2->x) && (ptr->y == ptr2->y)){
				//stop collided circles
				pthread_mutex_lock(&(ptr->mutex));
				pthread_mutex_lock(&(ptr2->mutex));
				ptr->collided = true;
				ptr2->collided = true;
				sleep(1);
				pthread_mutex_unlock(&(ptr2->mutex));
				pthread_mutex_unlock(&(ptr->mutex));
				ptr->collided = false;
				ptr2->collided = false;
				ptr->v_x *= 2;
				ptr->v_y *= 2;
				ptr2->v_x *= 2;
				ptr2->v_y *= 2;
			}
			ptr2 = ptr2->next;
		}

		//wait if collision
		pthread_mutex_lock(mutex_ptr);
		pthread_mutex_unlock(mutex_ptr);
	}
	ptr->x = -1;
	ptr->y = -1;
	ptr->visible = false;
	//circles = ptr->next;
}

void* draw(void* x){
	initscr();
	getmaxyx(stdscr, Y, X);
	curs_set(0);
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);

	timeout(1);
	while(!end){
		if(getch()=='x') end = true;
		clear();

		element* ptr = circles;
		while(ptr!=NULL){
			if(!(ptr->collided)){
				attron(COLOR_PAIR(1));
				mvprintw(ptr->y, ptr->x, "o");
			}
			else{
				attron(COLOR_PAIR(2));
				mvprintw(ptr->y, ptr->x, "+");
			}
			ptr = ptr->next;
		}

		refresh();
		usleep(40000);
	}
	endwin();
	element* ptr = circles;
	while(ptr!=NULL){
		element* temp = ptr;
		ptr = ptr->next;
		delete ptr;
	}
}

void* create_circles(void* ptr){
	usleep(100000);
	while(!end){
		pthread_t thread;
		float tab[2];

		int direction = rand()%7;
		tab[0] = 2 * v_factors[direction][0];
		tab[1] = 2 * v_factors[direction][1];
		
		pthread_create(&thread, NULL, circle, tab);
		sleep(1);
	}
}

int main(){


	pthread_t thread, thread2;
	pthread_create(&thread, NULL, draw, (void*)new int);
	pthread_create(&thread2, NULL, create_circles, (void*)new int);

	pthread_join(thread, NULL);
	pthread_join(thread2, NULL);

	return 0;
}
